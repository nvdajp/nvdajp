更新予定

A part of NonVisual Desktop Access (NVDA)
This file is covered by the GNU General Public License.
See the file COPYING for more details.
Copyright (C) 2015 Takuya Nishimoto


copy Japanese character symbols to subversion repo

jptools\copy_symbols_to_srt.cmd


checkCharDesc.py
====================================

character description consistency check

> cd jptools
> python checkCharDesc.py


checkSymbols.py
====================================

symbols consistency check

> cd jptools
> python checkSymbols.py


updateCharDesc.py
====================================

convert characters.dic to characterDescriptions.dic

> cd jptools
> python updateCharDesc.py > newfile.dic


